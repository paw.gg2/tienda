<!DOCTYPE html>
<html>

<head>
    <title>Productos</title>
    <style>
        * {
            background-color: #ADE1FB;

        }

        .todo {
            background-color: #ADE1FB;
        }

        td {
            width: 200px;
            text-align: center;
        }

        .imac {
            width: 120px;
            height: 100px;
            padding-left: 28px;
        }

        .iphone13 {
            width: 90px;
            height: 100px;
            padding-left: 61px;
        }

        .ipad {
            width: 120px;
            height: 100px;
            padding-left: 55px;
        }

        .reloj {
            width: 150px;
            height: 120px;
            /* padding-bottom: -10px; */
            padding-left: 27px;

        }

        .airpods {
            width: 120px;
            height: 90px;
            padding-left: 27px;

        }

        .pencil {
            width: 130px;
            height: 100px;
            padding-left: 34px;
        }

        .confirm {
            margin: 10px;
            float: left;

        }
    </style>
</head>

<body>
    <div class="todo">
        <?php
        session_start();

        // tras haberle dado al boton de ingresar guardamos en el array 'cesta' la cantidadidad de cada 
        // producto, inicianizandola a 0
        // Luego lo guardamos en una session dicho array cesta, y en otra sesion el usuarix
        if (isset($_POST['ingresar'])) {
            $cesta = array(
                'iMac' => 0, 'iPhone 13 Pro' => 0, 'iPad Mini' => 0,
                'Apple Watch 7' => 0, 'AirPods Pro' => 0, 'Apple Pencil' => 0
            );
            $_SESSION['cesta'] = $cesta;
            $_SESSION['usuarix'] = $_POST['usuarix'];
        }

        $productos = array(
            'iMac' => array('Producto' => "iMac", 'Descripcion' => "27 pulgadas", 'Precio' => 1999),
            'iPhone 13 Pro' => array('Producto' => "iPhone 13 Pro", 'Descripcion' => "128GB", 'Precio' => 1259),
            'iPad Mini' => array('Producto' => "iPad Mini", 'Descripcion' => "256GB", 'Precio' => 719),
            'Apple Watch 7' => array('Producto' => "Apple Watch 7", 'Descripcion' => "41 mm", 'Precio' => 449),
            'AirPods Pro' => array('Producto' => "AirPods Pro", 'Descripcion' => "Blanco", 'Precio' => 279),
            'Apple Pencil' => array('Producto' => "Apple Pencil", 'Descripcion' => "Bluetooth", 'Precio' => 135)
        );
        // var_dump($productos);
        // echo $productos['iMac']['Precio'];



        ?>

        <h2 style="padding:10px;">Bienvenido/a a la tienda, <?php echo $_SESSION['usuarix'] ?></h2>
        <div>
            <img class="imac" src="imac.png">
            <img class="iphone13" src="iphone13.png">
            <img class="ipad" src="ipad.png">
            <img class="reloj" src="reloj.png">
            <img class="airpods" src="airpods.png">
            <img class="pencil" src="pencil.png">
        </div>
        <?php


        echo "<form action='#' method='POST'>";
        echo "<table><tr>";
        foreach ($productos as $tipo => $producto) {
            echo "<td>";
            foreach ($producto as $dato => $valor) {
                echo "<p>$dato: $valor</p>";
            }
            echo "<button type='submit' style='background-color:white' name='añadir' value='$tipo'> Agregar</button>";
            echo "</td>";
        }
        echo "</tr></table><br/>";

        //Para añadir los productos a la cesta 
        if (isset($_POST['añadir'])) {
            $_SESSION['cesta'][$_POST['añadir']]++;
        }
        //Para eliminar los productos de la cesta 
        if (isset($_POST['eliminar'])) {
            if ($_SESSION['cesta'][$_POST['eliminar']] > 0) {
                $_SESSION['cesta'][$_POST['eliminar']]--;
            }
        }
        // entra en el if si la cantidadidad es distinta de 0
        if (
            $_SESSION['cesta']['iMac'] != 0 || $_SESSION['cesta']['iPhone 13 Pro'] != 0 ||
            $_SESSION['cesta']['iPad Mini'] != 0 || $_SESSION['cesta']['Apple Watch 7'] != 0 ||
            $_SESSION['cesta']['AirPods Pro'] != 0 || $_SESSION['cesta']['Apple Pencil'] != 0
        ) {
            echo "<h2 style='padding:10px'>Cesta</h2>";
            // para que me muestre el producto que he agregado:
            foreach ($_SESSION as $key => $producto) {
                if ($key == "cesta") {
                    foreach ($producto as $key => $valor) {
                        if ($valor != 0) {
                            echo "<span style='padding:10px'>$key: $valor</span> 
                            <button style='background-color:white'  type='submit' name='eliminar'
                             value ='$key'> Quitar</button><br/><br/>";
                        }
                    }
                }
            }
            //Guardamos en un array los precios de todos los productos
            $precios = array();
            foreach ($productos  as $key => $valor) {
                // echo $valor;
                foreach ($valor as $indice => $precio) {
                    if ($indice == "Precio") {
                        // echo $product;
                        $precios[$key] = $precio;
                    }
                }
            }
            //Para calcular el precio total de los productos
            $_SESSION['precio'] = $precios;
            $sumaTotal = 0;
            foreach ($_SESSION['cesta'] as $key => $cantidad) {
                if ($cantidad > 0) {
                    $precio = ($cantidad * $_SESSION['precio'][$key]);
                    $sumaTotal += $precio;
                }
            }
            // echo "sumaTotal: ".$sumaTotal;
            $_SESSION['sumaTotal'] = $sumaTotal;
        }
        echo "</form>";
        // en el momento que pulsemos añadir o eliminar nos saldrá el botón de confirmar
        if (isset($_POST['añadir']) || isset($_POST['eliminar'])) {
            echo "<form action='confirmar.php' method='POST'>";
            echo "<input class='confirm' style='background-color:white'   
        type='submit' name='compra' value='Confirmar Compra'>";
            echo "</form>";
        }
        ?>
        </form>
    </div>
</body>
</html>